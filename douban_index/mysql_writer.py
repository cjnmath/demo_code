import glob
import pandas as pd
import re
import mysql.connector


def create_connection():

    config = {
        'user': 'jc',
        'password': 'aller',
        'host': '127.0.0.1',
        'port': 3306,
        'raise_on_warnings': True
        }

    return mysql.connector.connect(database='douban_imdb_rating', **config)


def clean_nan(a):
    if str(a) == 'nan':
        return "[数据缺失]"
    else:
        return a


sql = """
INSERT INTO douban_index (movie_cast_cn,
                            movie_director_cn,
                            movie_douban_score,
                            movie_douban_url,
                            movie_name_cn,
                            movie_region_id)
VALUES (%s, %s, %s, %s, %s, %s);
"""

# selece all .csv file
for file in glob.glob('*.csv'):
    # get region_id
    movie_region_id = re.search(r'(?<=index_)\w+', file).group()
    df = pd.read_csv(file)
    # loop through every row  data and INSERT them to db
    for i in range(df.shape[0]):
        cnx = create_connection()
        cursor = cnx.cursor(buffered=True)
        movie_cast_cn = df.iloc[i]['movie_cast_cn']
        movie_director_cn = df.iloc[i]['movie_director_cn']
        movie_douban_score = float(df.iloc[i]['movie_douban_score'])
        # numpy cannt to convert to Mysql float directory
        movie_douban_url = df.iloc[i]['movie_douban_url']
        movie_name_cn = df.iloc[i]['movie_name_cn']
        values = (
                    movie_cast_cn,
                    movie_director_cn,
                    movie_douban_score,
                    movie_douban_url,
                    movie_name_cn,
                    movie_region_id)
        (
            movie_cast_cn,
            movie_director_cn,
            movie_douban_score,
            movie_douban_url,
            movie_name_cn,
            movie_region_id) = map(clean_nan, values)
        values = (
            movie_cast_cn,
            movie_director_cn,
            movie_douban_score,
            movie_douban_url,
            movie_name_cn,
            movie_region_id)
        cursor.execute(sql, values)
        cnx.commit()
        cursor.close()
        cnx.close()
        print('Finished inserting No.{} row of {}'.format(str(i), file))
