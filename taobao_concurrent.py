import time
import requests
import re
import json
from random import choice
from random import randint
from lxml.html import fromstring
from urllib.parse import urlparse
import threading


DESKTOP_USER_AGENTS = [
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like \
    Gecko) Chrome/61.0.3163.100 Safari/537.36 OPR/48.0.2685.52',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko\
    ) Chrome/62.0.3202.9 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like \
    Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063'
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
    Ubuntu Chromium/65.0.3325.181 Chrome/65.0.3325.181 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
    Chrome/61.0.3163.100 Safari/537.36',
    'Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/31.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 \
    (KHTML, like Gecko) Chrome/62.0.3202.75 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 \
    (KHTML, like Gecko) Chrome/60.0.3112.78 Safari/537.36 OPR/47.0.2631.55',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:56.0) Gecko/20100101 \
    Firefox/56.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/604.3.5 \
    (KHTML, like Gecko) Version/11.0.1 Safari/604.3.5'
]


def taobao_search(query=None, page_num=0):
    if not query:
        print('The searching query is None!')
        return None
    time_stamp = 1000 * time.time()
    time_slug = int(re.search('\.(\d+)', str(time_stamp)).group(1))
    search_url = 'https://s.taobao.com/search'
    payload = {
        'data-key': 's',
        'data-value': str(page_num * 44 + 44),
        'ajax': 'true',
        '_ksTS': str(int(time_stamp))+'_'+str(time_slug),
        'callback': 'jsonp'+str(time_slug+1),
        'q': query,
        'imgfile': '',
        'commend': 'all',
        'ssid': 's5-e',
        'search_type': 'item',
        'sourceId': 'tb.index',
        'spm': 'a21bo.2017.201856-taobao-item.1',
        'ie': 'utf8',
        'initiative_id': 'tbindexz_20170306',
        'bcoffset': '0',
        'ntoffset': '0',
        'p4ppushleft': '1,48',
    }
    if page_num:
        payload['s'] = str(page_num * 44)

    fake_headers = {
        'accept': '*/*',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7',
        'dnt': '1',
        'user-agent': choice(DESKTOP_USER_AGENTS)
    }
    session = requests.Session()
    results = []
    while not results:
        response = session.get(
                        search_url,
                        params=payload,
                        headers=fake_headers)
        try:
            raw_results = re.search(r'\((.*)\)', response.text).group(1)
            results = json.loads(
                        raw_results)['mods']['itemlist']['data']['auctions']
        except Exception as e:
            pass
        if not results:
            time.sleep(randint(1, 9))
    session.close()
    return results


def get_taobao_details(url):
    session = requests.Session()
    fake_headers = {
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,\
                    image/webp,image/apng,*/*;q=0.8',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7',
        'dnt': '1',
        'upgrade-insecure-requests': '1',
        'user-agent': choice(DESKTOP_USER_AGENTS)
    }
    print('downloading: ', url)
    response = session.get(url, headers=fake_headers)
    root = fromstring(response.text)

# find skuMap (which contains all price information) e.g.:
# {('1627207:1184723028', '5919063:3266779', '12304035:11835346', ):
#         {'oversold': False,
#         'price': '3599.00',
#         'skuId': '3832754959968',
#         'stock': '2'},}
    skuMap = {}
    for script in root.xpath('//script/text()'):
        if 'skuMap' in script:
            raw_skuMap = re.search(
                            'skuMap\s+:(.*)\s+,propertyMemoMap',
                            script).group(1)
            old_skuMap = json.loads(raw_skuMap)
            for old_key in old_skuMap.keys():
                key_list = old_key.split(';')
                new_key = tuple([i for i in key_list if i])
                skuMap[new_key] = old_skuMap[old_key]

# find properties, eg:
# {'套餐类型': {'套餐一': '5919063:3266779',
#           '套餐三': '5919063:3266785',
#           '套餐二': '5919063:3266781',
#           '套餐四': '5919063:3266786',
#           '官方标配': '5919063:6536025'},
#  '存储容量': {'128GB': '12304035:3222910',
#           '256GB': '12304035:11835346',
#           '64GB': '12304035:3222911'},
#  '机身颜色': {'白色【6+128G】+无线充电板': '1627207:1184723028',
#           '白色【6+64G】+无线充电板': '1627207:1184723033',
#           '白色【8+256G】+无线充电板': '1627207:1184723030',
#           '黑色【6+128G】+无线充电板': '1627207:1184723036',
#           '黑色【6+64G】+无线充电板': '1627207:1184723031',
#           '黑色【6+64G】-预售': '1627207:1793429635',
#           '黑色【8+256G】+无线充电板': '1627207:1184723034'},
#  '版本类型': {'中国大陆': '122216431:27772'}}
    properties = {}
    for property_block in root.xpath('//div[@class="tb-skin"]//dl[contains(@class, "J_Prop tb-prop tb-clear ")]'):
        property_name = property_block.xpath('./dt/text()')[0]
        choices = property_block.xpath('.//li//span/text()')
        choices_value = property_block.xpath('.//li/@data-value')
        properties[property_name] = dict(zip(choices, choices_value))

    return skuMap, properties


def get_tmall_details(url):
    session = requests.Session()
    fake_headers = {
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,\
                    image/webp,image/apng,*/*;q=0.8',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7',
        'dnt': '1',
        'upgrade-insecure-requests': '1',
        'user-agent': choice(DESKTOP_USER_AGENTS)
    }
    print('downloading: ', url)
    response = session.get(url, headers=fake_headers)
    root = fromstring(response.text)

    skuMap = {}
    for script in root.xpath('//script/text()'):
        if 'skuMap' in script:
            raw_skuMap = re.search(
                            '\"skuMap\":\s*(.*)},\s*\"valLoginIndicator\"',
                            script).group(1)
            old_skuMap = json.loads(raw_skuMap)
            for old_key in old_skuMap.keys():
                key_list = old_key.split(';')
                new_key = tuple([i for i in key_list if i])
                skuMap[new_key] = old_skuMap[old_key]

    properties = {}
    for property_block in root.xpath('//div[@class="tb-sku"]//dl[contains(@class, "tb-prop tm-sale-prop tm-clear ")]'):
        property_name = property_block.xpath('./dt/text()')[0]
        choices = property_block.xpath('.//li//span/text()')
        choices_value = property_block.xpath('.//li/@data-value')
        properties[property_name] = dict(zip(choices, choices_value))
    return skuMap, properties


def main():
    QUERY = 'thinkpad T470'
    basket = []

    def check_and_add(target_url):
        netloc = urlparse(target_url).netloc
        if 'taobao' in netloc:
            basket.append(get_taobao_details(target_url))
        elif 'tmall' in netloc:
            basket.append(get_tmall_details(target_url))

    target_urls = []
    for i in range(1):
        results = taobao_search(QUERY, i)
        for result in results:
            url = result['detail_url']
            if not url.startswith('http'):
                url = 'https:'+url
                target_urls.append(url)

    # multi thread
    max_thread_num = 20
    threads = []
    while target_urls:
        for thread in threads:
            if not thread.is_alive():
                threads.remove(thread)
        while len(threads) < max_thread_num and target_urls:
            target_url = target_urls.pop()
            thread = threading.Thread(
                        target=check_and_add,
                        kwargs={'target_url': target_url})
            thread.setDaemon(True)
            thread.start()
            threads.append(thread)
        for thread in threads:
            thread.join()

    # single thread
    # for target_url in target_urls:
    #     netloc = urlparse(target_url).netloc
    #     if 'taobao' in netloc:
    #         basket.append(get_taobao_details(target_url))
    #     elif 'tmall' in netloc:
    #         basket.append(get_tmall_details(target_url))

    return basket


if __name__ == '__main__':
    r = main()
