import scrapy
from urllib.parse import urlencode
from urllib.parse import urlparse
import re
import time
import json
from scrapy.loader import ItemLoader
from taobao_scrapy.items import TaobaoScrapyItem




class TaobaoSpider(scrapy.Spider):
    name = 'taobao'
    allowed_domains = ['taobao.com', 'tmall.com']

    def start_requests(self):
        query = "thinkpad T470"
        for page_num in range(2):
            time_stamp = 1000 * time.time()
            time_slug = int(re.search('\.(\d+)', str(time_stamp)).group(1))
            search_url = 'https://s.taobao.com/search?'
            payload = {
                'data-key': 's',
                'data-value': str(page_num * 44 + 44),
                'ajax': 'true',
                '_ksTS': str(int(time_stamp))+'_'+str(time_slug),
                'callback': 'jsonp'+str(time_slug+1),
                'q': query,
                'imgfile': '',
                'commend': 'all',
                'ssid': 's5-e',
                'search_type': 'item',
                'sourceId': 'tb.index',
                'spm': 'a21bo.2017.201856-taobao-item.1',
                'ie': 'utf8',
                'initiative_id': 'tbindexz_20170306',
                'bcoffset': '0',
                'ntoffset': '0',
                'p4ppushleft': '1,48',
            }
            if page_num:
                payload['s'] = str(page_num * 44)
            search_url = search_url + urlencode(payload)
            yield scrapy.Request(
                            url=search_url,
                            callback=self.extract_target_urls
                    )

    def extract_target_urls(self, response):
        results = []
        while not results:
            try:
                raw_results = re.search(r'\((.*)\)', response.text).group(1)
                results = json.loads(
                            raw_results)['mods']['itemlist']['data']['auctions']
            except Exception as e:
                pass
            if not results:
                # time.sleep(randint(1, 9))
                yield scrapy.Request(
                        url=response.url,
                        callback=self.extract_target_urls
                )

        for result in results:
            url = result['detail_url']
            if not url.startswith('http'):
                url = 'https:'+url
                netloc = urlparse(url).netloc
                if 'taobao' in netloc:
                    yield scrapy.Request(url=url, callback=self.extract_from_taobao)
                elif 'tmall' in netloc:
                    yield scrapy.Request(url=url, callback=self.extract_from_tmall)

    def extract_from_taobao(self, response):
        loader = ItemLoader(item=TaobaoScrapyItem(), response=response)

        skuMap = {}
        for script in response.xpath('//script/text()').extract():
            if 'skuMap' in script:
                raw_skuMap = re.search(
                                'skuMap\s+:(.*)\s+,propertyMemoMap',
                                script).group(1)
                old_skuMap = json.loads(raw_skuMap)
                for old_key in old_skuMap.keys():
                    key_list = old_key.split(';')
                    new_key = tuple([i for i in key_list if i])
                    skuMap[new_key] = old_skuMap[old_key]
        loader.add_value('skuMap', skuMap)

        properties = {}
        for property_block in response.xpath('//div[@class="tb-skin"]//dl[contains(@class, "J_Prop tb-prop tb-clear ")]'):
            property_name = property_block.xpath('./dt/text()').extract_first()
            choices = property_block.xpath('.//li//span/text()').extract()
            choices_value = property_block.xpath('.//li/@data-value').extract()
            properties[property_name] = dict(zip(choices, choices_value))
        loader.add_value('properties', properties)

        return loader.load_item()

    def extract_from_tmall(self, response):
        loader = ItemLoader(item=TaobaoScrapyItem(), response=response)

        skuMap = {}
        for script in response.xpath('//script/text()').extract():
            if 'skuMap' in script:
                raw_skuMap = re.search(
                                '\"skuMap\":\s*(.*)},\s*\"valLoginIndicator\"',
                                script).group(1)
                old_skuMap = json.loads(raw_skuMap)
                for old_key in old_skuMap.keys():
                    key_list = old_key.split(';')
                    new_key = tuple([i for i in key_list if i])
                    skuMap[new_key] = old_skuMap[old_key]
        loader.add_value('skuMap', skuMap)

        properties = {}
        for property_block in response.xpath('//div[@class="tb-sku"]//dl[contains(@class, "tb-prop tm-sale-prop tm-clear ")]'):
            property_name = property_block.xpath('./dt/text()').extract_first()
            choices = property_block.xpath('.//li//span/text()').extract()
            choices_value = property_block.xpath('.//li/@data-value').extract()
            properties[property_name] = dict(zip(choices, choices_value))
        loader.add_value('properties', properties)

        return loader.load_item()
