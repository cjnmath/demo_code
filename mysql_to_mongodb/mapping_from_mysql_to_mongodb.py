import mysql.connector
from pymongo import MongoClient

# from pymongo import UpdateOne


# MariaDB [douban_imdb_rating]> DESCRIBE douban_to_imdb;
# +------------+---------------+------+-----+---------+-------+
# | Field      | Type          | Null | Key | Default | Extra |
# +------------+---------------+------+-----+---------+-------+
# | douban_url | varchar(2083) | YES  |     | NULL    |       |
# | imdb_url   | varchar(2083) | YES  |     | NULL    |       |
# +------------+---------------+------+-----+---------+-------+

config = {
    'user': 'jc',
    'password': 'aller',
    'host': '127.0.0.1',
    'port': 3306,
    'raise_on_warnings': True
}

cnx = mysql.connector.connect(database='douban_imdb_rating', **config)
cursor = cnx.cursor()
query = "SELECT * FROM douban_to_imdb"

cursor.execute(query)

# for (movie_cast_cn, movie_director_cn) in cursor:
#     print(movie_cast_cn.split(','), movie_director_cn.split(','))
client = MongoClient()
db = client.douban_imdb_rating
collection = db.all_data
n = 0
for (douban_url, imdb_url) in cursor:
    # collection.bulk_write([
    #     UpdateOne(
    #         {'movie_douban_url': douban_url},
    #         {'$set': {'movie_imdb_url': imdb_url}})
    # ])
    result = collection.update_one(
        {'movie_douban_url': douban_url},
        {'$set': {'movie_imdb_url': imdb_url}},
        upsert=True)
    print(douban_url, imdb_url)
    print(result.modified_count)
    n += 1
    print(n)
cursor.close()
cnx.close()
