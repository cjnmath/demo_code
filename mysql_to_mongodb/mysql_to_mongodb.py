import mysql.connector
from pymongo import MongoClient

# MariaDB [douban_imdb_rating]> DESCRIBE douban_index ;
# +--------------------+---------------+------+-----+---------+-------+
# | Field              | Type          | Null | Key | Default | Extra |
# +--------------------+---------------+------+-----+---------+-------+
# | movie_cast_cn      | text          | YES  |     | NULL    |       |
# | movie_director_cn  | text          | YES  |     | NULL    |       |
# | movie_douban_score | varchar(10)   | YES  |     | NULL    |       |
# | movie_douban_url   | varchar(2083) | YES  |     | NULL    |       |
# | movie_name_cn      | text          | YES  |     | NULL    |       |
# | movie_region_id    | varchar(5)    | YES  |     | NULL    |       |
# +--------------------+---------------+------+-----+---------+-------+
config = {
    'user': 'jc',
    'password': 'aller',
    'host': '127.0.0.1',
    'port': 3306,
    'raise_on_warnings': True
}

cnx = mysql.connector.connect(database='douban_imdb_rating', **config)
cursor = cnx.cursor()

query = "SELECT * FROM duplicated_douban_index"
# query = "SELECT {}, {} FROM douban_index LIMIT {} "

cursor.execute(query)

# for (movie_cast_cn, movie_director_cn) in cursor:
#     print(movie_cast_cn.split(','), movie_director_cn.split(','))
client = MongoClient()
db = client.douban_imdb_rating
collection = db.all_data
n = 0

for (movie_cast_cn, movie_director_cn, movie_douban_score, movie_douban_url,
        movie_name_cn, movie_region_id) in cursor:
    data = {
        'movie_cast_cn': movie_cast_cn.split(','),
        'movie_director_cn': movie_director_cn.split(','),
        'movie_douban_score': movie_douban_score,
        'movie_douban_url': movie_douban_url,
        'movie_name_cn': movie_name_cn,
        'movie_region_id': movie_region_id
    }
    collection.insert_one(data)
    n += 1
    print(n)
cursor.close()
cnx.close()
